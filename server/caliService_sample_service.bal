import ballerina/grpc;
import ballerina/log;
import ballerina/io;
import ballerina/crypto;
import ballerina/stringutils;

listener grpc:Listener ep = new (9090);

RecordInfo info_2 = {
	date: "12/09/2020",
	artists: [
		{
		   name: "Winston Marshall",
		   member: "yes"
		},
		{
		   name: "Ben Lovett",
		   member: "no"
		}
	],
	band: "Lumineers",
	songs: [
		{
			title: "Stubborn Love",
			genre: "indie",
			platform: "Deezer"
		}
	],
	key: "0c085aff8e293cb6e3d647ab1dbd6b80",
	song_version: 1
};

RecordInfo info_3 = {
	date: "10/05/2020",
	artists: [
		{
		   name: "ABC",
		   member: "yes"
		},
		{
		   name: "Ben Lovett",
		   member: "yes"
		}
	],
	band: "Chainsmokers",
	songs: [
		{
			title: "There will be time",
			genre: "folk rock",
			platform: "Deezer"
		}
	],
	key: "7cdaa9ae4d7f5e7b5aac6670ed38f274",
	song_version: 1
};


//where the songs willbe saved
map <RecordInfo> savedRecords = {"0c085aff8e293cb6e3d647ab1dbd6b80":info_2, "7cdaa9ae4d7f5e7b5aac6670ed38f274":info_3};

service caliService on ep {

    resource function writeRecord(grpc:Caller caller, RecordInfo value) {
        
		io:println(" --------- Starting Writing Service ------------ ");
		
		//if song comes with key already that means we are updation
		if(value.key == ""){
			byte[] inputArr = value.toString().toBytes();
			byte[] output = crypto:hashMd5(inputArr);
			io:println("KEY encoded hash with MD5: " + output.toBase16());
			
			string newKey = output.toBase16().toString();
			
			boolean found = savedRecords.hasKey(newKey);
			
			if(found == false){
				//save the song
				
				hashCode response = {
					key: newKey,
					song_version: 1
				};
				
				error? result_ = caller->send(response);
				
				value.key = newKey;
				value.song_version = 1;
				savedRecords[newKey] = <@untainted> value;	
				
			}else{
				//already exits
				hashCode response = {
					key: savedRecords.get(newKey).key,
					song_version: savedRecords.get(newKey).song_version
				};
				
				grpc:Error? result = caller->send(response);
				
				if (result is grpc:Error) {
					log:printError("Error occured when sending the hashCode.");
				} else {
					log:printInfo ("Successfully sent " + response.key.toString() + " for " + response.song_version.toString());
				}			
				
				result = caller->complete();
				if (result is grpc:Error) {
					log:printError("Error occured when closing the connection. ");
					}
				
			}
		}else{				
			//increment the version
			value.song_version = value.song_version + 1;
						
			//add new modified song to map,this overwrites
			savedRecords[value.key] = <@untainted> value;
			
			hashCode response = {
				key: value.key,
				song_version: value.song_version
			};
			
			error? result_ = caller->send(response);
		}
		
		// Implementation goes here.

        // You should return a hashCode
		
    }
	
    resource function updateRecord(grpc:Caller caller, hashCode value) {
        boolean found = savedRecords.hasKey(value.key);
		
		if(found == true){
			
			//get the song from the map
			var songFound = savedRecords.get(value.key);	
			
			//send the song back for editing
			grpc:Error? result = caller->send(songFound);
			
		}else{
			//doesnt exist
			io:println("Song trying to update does not exist");
			
			hashCode response = {
				key: "Song trying to update does not exist"
			};			
			grpc:Error? result = caller->send(response);
		}
		
		// Implementation goes here.

        // You should return a RecordInfo
    }
	
    @grpc:ResourceConfig { streaming: true } 
    resource function viewRecord(grpc:Caller caller, criterion value) {
        io:println(" --------- Starting Reading Service ------------ ");
		
		string searchTerm = value.name.toString();
		
		foreach var song in savedRecords{
			
			if( (stringutils:contains(song.songs.toString(), searchTerm)) || (stringutils:contains(song.artists.toString(), searchTerm)) || (stringutils:contains(song.band.toString(), searchTerm)) || (stringutils:contains(song.date.toString(), searchTerm))){
				
				var song_send = savedRecords.get(song.key.toString());
				
				io:println();
				io:println("=============== SERVER SENDING SONG FOR READING ===============");
				io:println("KEY     : ", song_send.key);
				io:println("VERSION : ", song_send.song_version);
				io:println("DATE    : ", song_send.date);
				io:println("ARTISTS : ", song_send.artists);
				io:println("BAND    : ", song_send.band);
				io:println("SONGS   : ", song_send.songs);
				io:println("================================================================");
				
				grpc:Error? err = caller->send(song_send.toString());
				
					
				
				if (err is grpc:Error) {
					log:printError("Error from Connector: " + err.reason() + " - ");
				} else {
					log:printInfo("Successfully Sent ");
				}
			} 
		}
		
		//Once all the messages are sent, the server notifies the caller with a `complete` message.
		grpc:Error? result = caller->complete();
		
		if (result is grpc:Error) {
            log:printError("Error in sending completed notification to caller",
			err = result);
        }
		
		// Implementation goes here.
        // You should return a RecordInfo
    }
}

public type Songs record {|
    string title = "";
    string genre = "";
    string platform = "";
    
|};

public type Artists record {|
    string name = "";
    string member = "";
    
|};

public type RecordInfo record {|
    string date = "";
    Artists[] artists = [];
    string band = "";
    Songs[] songs = [];
    string key = "";
    int song_version = 0;
    
|};

public type hashCode record {|
    string key = "";
    int song_version = 0;
    
|};

public type criterion record {|
    string name = "";
    
|};



const string ROOT_DESCRIPTOR = "0A0C4275666665722E70726F746F120773657276696365224F0A05536F6E677312140A057469746C6518012001280952057469746C6512140A0567656E7265180220012809520567656E7265121A0A08706C6174666F726D1803200128095208706C6174666F726D22350A074172746973747312120A046E616D6518012001280952046E616D6512160A066D656D62657218022001280952066D656D62657222BB010A0A5265636F7264496E666F12120A0464617465180120012809520464617465122A0A076172746973747318022003280B32102E736572766963652E4172746973747352076172746973747312120A0462616E64180320012809520462616E6412240A05736F6E677318042003280B320E2E736572766963652E536F6E67735205736F6E677312100A036B657918052001280952036B657912210A0C736F6E675F76657273696F6E180620012803520B736F6E6756657273696F6E223F0A0868617368436F646512100A036B657918012001280952036B657912210A0C736F6E675F76657273696F6E180220012803520B736F6E6756657273696F6E221F0A09637269746572696F6E12120A046E616D6518012001280952046E616D6532B5010A0B63616C695365727669636512350A0B77726974655265636F726412132E736572766963652E5265636F7264496E666F1A112E736572766963652E68617368436F646512360A0C7570646174655265636F726412112E736572766963652E68617368436F64651A132E736572766963652E5265636F7264496E666F12370A0A766965775265636F726412122E736572766963652E637269746572696F6E1A132E736572766963652E5265636F7264496E666F3001620670726F746F33";
function getDescriptorMap() returns map<string> {
    return {
        "Buffer.proto":"0A0C4275666665722E70726F746F120773657276696365224F0A05536F6E677312140A057469746C6518012001280952057469746C6512140A0567656E7265180220012809520567656E7265121A0A08706C6174666F726D1803200128095208706C6174666F726D22350A074172746973747312120A046E616D6518012001280952046E616D6512160A066D656D62657218022001280952066D656D62657222BB010A0A5265636F7264496E666F12120A0464617465180120012809520464617465122A0A076172746973747318022003280B32102E736572766963652E4172746973747352076172746973747312120A0462616E64180320012809520462616E6412240A05736F6E677318042003280B320E2E736572766963652E536F6E67735205736F6E677312100A036B657918052001280952036B657912210A0C736F6E675F76657273696F6E180620012803520B736F6E6756657273696F6E223F0A0868617368436F646512100A036B657918012001280952036B657912210A0C736F6E675F76657273696F6E180220012803520B736F6E6756657273696F6E221F0A09637269746572696F6E12120A046E616D6518012001280952046E616D6532B5010A0B63616C695365727669636512350A0B77726974655265636F726412132E736572766963652E5265636F7264496E666F1A112E736572766963652E68617368436F646512360A0C7570646174655265636F726412112E736572766963652E68617368436F64651A132E736572766963652E5265636F7264496E666F12370A0A766965775265636F726412122E736572766963652E637269746572696F6E1A132E736572766963652E5265636F7264496E666F3001620670726F746F33"
        
    };
}

