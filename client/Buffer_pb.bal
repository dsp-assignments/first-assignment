import ballerina/grpc;

public type caliServiceBlockingClient client object {

    *grpc:AbstractClientEndpoint;

    private grpc:Client grpcClient;

    public function __init(string url, grpc:ClientConfiguration? config = ()) {
        // initialize client endpoint.
        self.grpcClient = new(url, config);
        checkpanic self.grpcClient.initStub(self, "blocking", ROOT_DESCRIPTOR, getDescriptorMap());
    }

    public remote function writeRecord(RecordInfo req, grpc:Headers? headers = ()) returns ([hashCode, grpc:Headers]|grpc:Error) {
        
        var payload = check self.grpcClient->blockingExecute("service.caliService/writeRecord", req, headers);
        grpc:Headers resHeaders = new;
        anydata result = ();
        [result, resHeaders] = payload;
        
        return [<hashCode>result, resHeaders];
        
    }

    public remote function updateRecord(hashCode req, grpc:Headers? headers = ()) returns ([RecordInfo, grpc:Headers]|grpc:Error) {
        
        var payload = check self.grpcClient->blockingExecute("service.caliService/updateRecord", req, headers);
        grpc:Headers resHeaders = new;
        anydata result = ();
        [result, resHeaders] = payload;
        
        return [<RecordInfo>result, resHeaders];
        
    }

    public remote function viewRecord(criterion req, service msgListener, grpc:Headers? headers = ()) returns (grpc:Error?) {
        
        return self.grpcClient->nonBlockingExecute("service.caliService/viewRecord", req, msgListener, headers);
    }

};

public type caliServiceClient client object {

    *grpc:AbstractClientEndpoint;

    private grpc:Client grpcClient;

    public function __init(string url, grpc:ClientConfiguration? config = ()) {
        // initialize client endpoint.
        self.grpcClient = new(url, config);
        checkpanic self.grpcClient.initStub(self, "non-blocking", ROOT_DESCRIPTOR, getDescriptorMap());
    }

    public remote function writeRecord(RecordInfo req, service msgListener, grpc:Headers? headers = ()) returns (grpc:Error?) {
        
        return self.grpcClient->nonBlockingExecute("service.caliService/writeRecord", req, msgListener, headers);
    }

    public remote function updateRecord(hashCode req, service msgListener, grpc:Headers? headers = ()) returns (grpc:Error?) {
        
        return self.grpcClient->nonBlockingExecute("service.caliService/updateRecord", req, msgListener, headers);
    }

    public remote function viewRecord(criterion req, service msgListener, grpc:Headers? headers = ()) returns (grpc:Error?) {
        
        return self.grpcClient->nonBlockingExecute("service.caliService/viewRecord", req, msgListener, headers);
    }

};

public type Songs record {|
    string title = "";
    string genre = "";
    string platform = "";
    
|};


public type Artists record {|
    string name = "";
    string member = "";
    
|};


public type RecordInfo record {|
    string date = "";
    Artists[] artists = [];
    string band = "";
    Songs[] songs = [];
    string key = "";
    int song_version = 0;
    
|};


public type hashCode record {|
    string key = "";
    int song_version = 0;
    
|};


public type criterion record {|
    string name = "";
    
|};



const string ROOT_DESCRIPTOR = "0A0C4275666665722E70726F746F120773657276696365224F0A05536F6E677312140A057469746C6518012001280952057469746C6512140A0567656E7265180220012809520567656E7265121A0A08706C6174666F726D1803200128095208706C6174666F726D22350A074172746973747312120A046E616D6518012001280952046E616D6512160A066D656D62657218022001280952066D656D62657222BB010A0A5265636F7264496E666F12120A0464617465180120012809520464617465122A0A076172746973747318022003280B32102E736572766963652E4172746973747352076172746973747312120A0462616E64180320012809520462616E6412240A05736F6E677318042003280B320E2E736572766963652E536F6E67735205736F6E677312100A036B657918052001280952036B657912210A0C736F6E675F76657273696F6E180620012803520B736F6E6756657273696F6E223F0A0868617368436F646512100A036B657918012001280952036B657912210A0C736F6E675F76657273696F6E180220012803520B736F6E6756657273696F6E221F0A09637269746572696F6E12120A046E616D6518012001280952046E616D6532B5010A0B63616C695365727669636512350A0B77726974655265636F726412132E736572766963652E5265636F7264496E666F1A112E736572766963652E68617368436F646512360A0C7570646174655265636F726412112E736572766963652E68617368436F64651A132E736572766963652E5265636F7264496E666F12370A0A766965775265636F726412122E736572766963652E637269746572696F6E1A132E736572766963652E5265636F7264496E666F3001620670726F746F33";
function getDescriptorMap() returns map<string> {
    return {
        "Buffer.proto":"0A0C4275666665722E70726F746F120773657276696365224F0A05536F6E677312140A057469746C6518012001280952057469746C6512140A0567656E7265180220012809520567656E7265121A0A08706C6174666F726D1803200128095208706C6174666F726D22350A074172746973747312120A046E616D6518012001280952046E616D6512160A066D656D62657218022001280952066D656D62657222BB010A0A5265636F7264496E666F12120A0464617465180120012809520464617465122A0A076172746973747318022003280B32102E736572766963652E4172746973747352076172746973747312120A0462616E64180320012809520462616E6412240A05736F6E677318042003280B320E2E736572766963652E536F6E67735205736F6E677312100A036B657918052001280952036B657912210A0C736F6E675F76657273696F6E180620012803520B736F6E6756657273696F6E223F0A0868617368436F646512100A036B657918012001280952036B657912210A0C736F6E675F76657273696F6E180220012803520B736F6E6756657273696F6E221F0A09637269746572696F6E12120A046E616D6518012001280952046E616D6532B5010A0B63616C695365727669636512350A0B77726974655265636F726412132E736572766963652E5265636F7264496E666F1A112E736572766963652E68617368436F646512360A0C7570646174655265636F726412112E736572766963652E68617368436F64651A132E736572766963652E5265636F7264496E666F12370A0A766965775265636F726412122E736572766963652E637269746572696F6E1A132E736572766963652E5265636F7264496E666F3001620670726F746F33"
        
    };
}

