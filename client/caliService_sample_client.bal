import ballerina/io;
import ballerina/grpc;

RecordInfo info_1 = {
	date: "22/10/2020",
	artists: [
		{
		   name: "Winston Marshall",
		   member: "yes"
		},
		{
		   name: "Ben Lovett",
		   member: "yes"
		},
		{
		   name: "Baaba Maal",
		   member: "no"
		}
	],
	band: "Mumford & Sons",
	songs: [
		{
			title: "There will be time",
			genre: "folk rock",
			platform: "Deezer"
		}
	]
};

public function main (string... args) returns error?{

    caliServiceBlockingClient blockingEp = new("http://localhost:9090");
	caliServiceClient non_blockingEp = new("http://localhost:9090");

	io:println("__________ GRPC  MENU _____________");
    io:println("1. ADD SONG");
	io:println("2. UPDATE SONG");
	io:println("3. VIEW SONG");
	io:println("___________________________________");
	var choice = io:readln("Enter Choice : ");
	
	if(choice == "1"){
	
		[hashCode, grpc:Headers] result = check blockingEp->writeRecord(info_1);
		
		io:println("GENERATED KEY : ", result[0].key );
		io:println("VERSION       : ", result[0].song_version );
		
	}else if(choice == "2"){
		hashCode update_info = {
			key: "0c085aff8e293cb6e3d647ab1dbd6b80",
			song_version: 1
		};
		
		[RecordInfo, grpc:Headers] result = check blockingEp->updateRecord(update_info);		
		RecordInfo retrievedSong = result[0];
		
		io:println("================= RECEIVED SONG FOR UPDATING ====================");
		io:println("KEY     : ", retrievedSong.key);
		io:println("VERSION : ", retrievedSong.song_version);
		io:println("DATE    : ", retrievedSong.date);
		io:println("ARTISTS : ", retrievedSong.artists);
		io:println("BAND    : ", retrievedSong.band);
		io:println("SONGS   : ", retrievedSong.songs);
		io:println("================================================================");	
		
		io:println();
		
		//make changes		
		io:println("__________ MODIFY  MENU _____________");
		io:println("1. DATE");
		io:println("2. BAND");
		io:println("___________________________________");
		var modify_choice = io:readln("Enter Choice : ");
		
		if(modify_choice == "1"){
			retrievedSong.date = io:readln("Enter new date : ");
			
		}else if(modify_choice == "2"){
			retrievedSong.band = io:readln("Enter band name : ");			
		}
						
		//sent it to be written now
		[hashCode, grpc:Headers] result_hash = check blockingEp->writeRecord(retrievedSong);
		hashCode hash_info = result_hash[0];
		
		io:println();
		io:println("OLD GENERATED KEY : ", hash_info.key );
		io:println("NEW SONG VERSION  : ", hash_info.song_version );
		
	}else if(choice == "3"){
		//WHY ITS NOT WORKING
		//OPEN ISSUE ON BALLERINA --> https://github.com/ballerina-platform/ballerina-standard-library/issues/388
		var input = io:readln("Enter your search Criterion: ");
		
		criterion  searchInput = {
			name: input
		};
		
		grpc:Error? result = non_blockingEp->viewRecord(searchInput,MessageListener);	
		
		if (result is grpc:Error) {
			io:println("Error from Connector: " + result.reason());
		} else {
			io:println("Connected successfully");
		}
		
	}else{
		io:println("Incorrect choice entered");
	}
	
}

// Message listener for incoming messages
service MessageListener = service {

    resource function onMessage(RecordInfo message) {
        io:println(" ****** Received a Song ******");
		
		io:println();
		io:println(message);
		io:println();
		io:println(" ********************************");
    }

    resource function onError(error err) {
		io:println("Error reported from server: " + err.reason() );
    }

    resource function onComplete() {
        io:println("  completed");
    }
};


